(set-prefix-key (kbd "s-c"))


(load (concstring *confdir* "programs.lisp"))
(load (concstring *confdir* "commands.lisp"))

(defun no-prefix (key function)
  (define-key *top-map* (kbd key) function))

(defun prefix (key function)
  (define-key *root-map* (kbd key) function))


(prefix  	"0" 		"remove")
(prefix  	"1" 		"only")
(prefix  	"2" 		"vsplit")
(prefix  	"3" 		"hsplit")

(prefix  	"O" 		"fprev")

(no-prefix	"s-TAB"		"pull-hidden-next")
(no-prefix	"M-`"		"fother")
(no-prefix	"s-q"		"delete")
(no-prefix	"C-F5"		"pull-hidden-previous")
(no-prefix	"C-F6"		"pull-hidden-next")
(no-prefix	"C-F7"		"fprev")
(no-prefix	"C-F8"		"fnext")
(no-prefix	"M-F5"		"gprev")
(no-prefix	"M-F6"		"gnext")
(no-prefix	"M-TAB"		"gother")
(no-prefix	"M-F7"		"gnew")
(no-prefix	"M-F8"		"gnew-float")
(prefix		"s-g"		"grouplist")

(no-prefix	"s-1" 		"gselect 1")
(no-prefix	"s-2" 		"gselect 2")
(no-prefix	"s-3" 		"gselect 3")
(no-prefix	"s-4" 		"gselect 4")
(no-prefix	"s-5" 		"gselect 5")
(no-prefix	"s-6" 		"gselect 6")
(no-prefix	"s-7" 		"gselect 7")
(no-prefix	"s-8" 		"gselect 8")
(no-prefix	"s-9" 		"gselect 9")
(no-prefix	"s-0" 		"gselect 10")

(prefix	       "b" 		"select")
(prefix	       "s-b" 		"windowlist")
(prefix	       "s-Up" 		"move-window up")
(prefix	       "s-Down"		"move-window down")
(prefix	       "s-Left"		"move-window left")
(prefix	       "s-Right"	"move-window right")

(no-prefix "s-!" "gmove-and-follow 1")
(no-prefix "s-@" "gmove-and-follow 2")
(no-prefix "s-#" "gmove-and-follow 3")
(no-prefix "s-$" "gmove-and-follow 4")
(no-prefix "s-%" "gmove-and-follow 5")
(no-prefix "s-^" "gmove-and-follow 6")
(no-prefix "s-&" "gmove-and-follow 7")
(no-prefix "s-*" "gmove-and-follow 8")
(no-prefix "s-(" "gmove-and-follow 9")
(no-prefix "s-)" "gmove-and-follow 0")


(no-prefix     "s-w" 		"move-focus up")
(no-prefix     "s-s" 		"move-focus down")
(no-prefix     "s-a" 		"move-focus left")
(no-prefix     "s-d" 		"move-focus right")
(no-prefix     "s-Up" 		"move-focus up")
(no-prefix     "s-Down"		"move-focus down")
(no-prefix     "s-Left"		"move-focus left")
(no-prefix     "s-Right"	"move-focus right")
(no-prefix     "s-p" 		"move-focus up")
(no-prefix     "s-n" 		"move-focus down")
(no-prefix     "s-b" 		"move-focus left")
(no-prefix     "s-f" 		"move-focus right")

(no-prefix     "s-B" 		"gprev")
(no-prefix     "s-F" 		"gnext")
(no-prefix     "M-s-a" 		"gprev")
(no-prefix     "M-s-d" 		"gnext")
(no-prefix     "M-s-Left"	"gprev")
(no-prefix     "M-s-Right"	"gnext")

(no-prefix     "s-x" 		"colon")
(no-prefix     "s-M-x" 		"eval")
(no-prefix     "s-z" 		"exec")


(prefix	       "c"        	"c2e")
(prefix	       "C"		"eaacm")


(prefix        "s-l" 		"tml")
(no-prefix     "H-p"		"tml")

(no-prefix     "C-M-t"		 *term*)
(no-prefix     "s-e"		 *file-manager*)
(no-prefix     "M-F2"		 *menu*)
(no-prefix     "M-F3"		 *window-switcher*)

(prefix        "P"		"run-shell-command cmus-info-notify")
(prefix        "E"		"run-shell-command emacsclient -c")

(no-prefix "XF86ScreenSaver" "exec slock")

(prefix	       "s-e"		"emacs-anywhere")

(prefix	       "s-r"		"send-raw-key")

(define-remapped-keys
    `((,(lambda (win)
          (or
	   (string-equal "Discord" (window-class win))
	   (string-equal "IceCat" (window-class win))
	   (string-equal "Nightly" (window-class win))
	   (string-equal "KeePassXC" (window-class win))
	   (string-equal "Riot" (window-class win))
	   ;; (string-equal "Freeciv" (window-class win))
	   (string-equal "Firefox" (window-class win))))
	("C-n"   . "Down")
	("C-p"   . "Up")
	("C-f"   . "Right")
	("C-b"   . "Left")
	("C-v"   . "Next")
	("M-v"   . "Prior")
	("M-w"   . "C-c")
	("C-S-w" . "M-w")
	("C-w"   . "C-x")
	("C-y"   . "C-v")
	("M-<"   . "Home")
	("M->"   . "End")
	("C-M-b" . "M-Left")
	("C-M-f" . "M-Right")
	("C-k"   . ("C-S-End" "C-x"))
	("C-g"   . "ESC")
	("C-M-b" . "XF86Back")
	("C-M-f" . "XF86Forward")
	("C-o" 	 . ("RET" "Up"))
	("C-/"   . "C-z")
	("C-M-/" . "C-S-z")
	("C-s"   . "C-f")
	("C-m"   . "RET")
	("C-M-m" . "C-m")
	("C-a"   . "Home")
	("C-e"   . "End")
	("C-M-h" . "C-a")
	("C-h"   . "DEL")
	("M-h"   . "C-DEL")
	("C-d"   . "Delete")
	("M-d"   . "C-Delete")
	("C-M-k" . "C-k")
	("C-M-n" . "C-n")
	("C-M-w" . "C-w")
	("M-f"   . "C-Right")
	("M-b"   . "C-Left")
	)))



;; Local Variables:
;; eval: (stumpwm-mode)
;; eval: (paredit-mode)
;; End:
