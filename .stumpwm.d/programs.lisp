(setf *browser* "exec firefox")
(setf *term* "exec st -e tmux")
(setf *file-manager* "exec caja")
(setf *menu* "exec rofi -show run -lines 10 -font 'Iosevka 12' -location 1 -theme solarized &")
(setf *window-switcher* "exec rofi -show window -lines 10 -font 'Iosevka 12' -location 1 -theme solarized &")




;; Local Variables:
;; eval: (stumpwm-mode)
;; eval: (paredit-mode)
;; End:
