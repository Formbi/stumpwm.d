(in-package :stumpwm)

(load (concstring *confdir* "programs.lisp"))

(defcommand weechat-group () ()
  (run-shell-command (concstring *confdir* "bash/weechat-group")))

(defcommand net-group () ()
  (run-shell-command (concstring *confdir* "bash/net-group")))

(defcommand fm-group () ()
  (run-shell-command (concstring *confdir* "bash/fm-group")))

(defcommand emacs-group () ()
  (run-shell-command (concstring *confdir* "bash/emacs-group")))

(defcommand media-group () ()
  (run-shell-command (concstring *confdir* "bash/media-group")))

(defcommand wot-group () ()
  (run-shell-command (concstring *confdir* "bash/wot-group")))

(defcommand gimp-group () ()
  (run-shell-command (concstring *confdir* "bash/gimp-group")))


(defcommand tml () () (stumpwm:toggle-mode-line (stumpwm:current-screen)
                          (stumpwm:current-head)))

(defcommand c2e () ()
  "Copy the X clipboard contents to the Emacs kill ring."
  (run-shell-command "c2e"))

(defcommand eaacm () ()
  "Emacs as a clipboard manager."
  ;; (run-or-raise "emacsclient -nc" '(:class "Emacs"))
  (run-shell-command "emacsclient -c -e '(progn (let ((helm-full-frame t)) \
    (save-window-excursion (delete-other-windows) (helm-show-kill-ring))) (delete-frame))'"))

(defcommand emacs-anywhere () ()
	    "launch emacs-anywhere on the current textarea"
	    (sleep 1) ;; doing stuff too fast makes a key stuck as pressed
	    (meta (kbd "C-a"))
	    (sleep 0.5)
	    (meta (kbd "C-c"))
	    (sleep 0.5)
	    (run-shell-command "c2e")
	    (run-shell-command "~/.emacs_anywhere/bin/run"))


;; Local Variables:
;; eval: (stumpwm-mode)
;; eval: (paredit-mode)
;; End:
