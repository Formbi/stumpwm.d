;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Joel Agnel Fernandes
;; initramd@gmail.com
;;
;; Description: Multimedia key mappings for stumpwm
;; suggestions/patches welcomed
;;
;; Usage: Simply load this file and go
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(in-package :stumpwm)


(defun no-prefix (key function)
  (define-key *top-map* (kbd key) function))

(defun prefix (key function)
  (define-key *root-map* (kbd key) function))


;; KEY DEFINITION - Key code symbol table
;; note: certain keyboards have this different. use xev to find yours if these don't work
(setf *key-codes*
      '((162 . "XF86AudioPlay")		   ; handled by amarok (or other mp3 players)
	(164 . "XF86AudioStop")		   
	(144 . "XF86AudioPrev")
	(153 . "XF86AudioNext")
	(160 . "XF86AudioMute")
	(174 . "XF86AudioLowerVolume")	   ; we use amixer (alsa mixer) to  handle this
	(176 . "XF86AudioRaiseVolume")))

;; Map keycodes to keysyms
(mapcar (lambda (pair)
	  (let* ((keycode (car pair))
		 (keysym  (cdr pair))
		 (format-dest nil)
		 (format-dest (make-array 5 :fill-pointer 0 :adjustable t :element-type 'character)))
	    (format format-dest "xmodmap -e 'keycode ~d = ~a'" keycode keysym)
	    (run-shell-command format-dest)
	  format-dest))
	*key-codes*)

;; Volume control
(no-prefix 	"XF86AudioLowerVolume"	      "exec amixer set Master 5%-")
(no-prefix	"XF86AudioRaiseVolume" 	      "exec amixer set Master 5%+")

(no-prefix      "XF86AudioPlay"		      "exec mpc toggle")
(prefix 	"XF86AudioPlay"		      "exec mpd-now-playing-notify")

(no-prefix 	"XF86AudioStop" 	      "exec cmus-remote -s")

(prefix		"XF86AudioPrev"		       "exec cmus-remote -r")
(prefix		"XF86AudioNext"		       "exec cmus-remote -n")

;; (no-prefix 	"XF86AudioPrev" 	       "exec cmus-remote -k -30")
(no-prefix 	"XF86AudioPrev" 	       "exec mpc seek -00:00:30")

;; (no-prefix 	"XF86AudioNext"		       "exec cmus-remote -k +30")
(no-prefix 	"XF86AudioNext"		       "exec mpc seek +00:00:30")

;; Local Variables:
;; eval: (stumpwm-mode)
;; eval: (paredit-mode)
;; End:
