(in-package :stumpwm)

(defun concstring (a b) (concatenate 'string a b))
(setf *home* (uiop:getenv "HOME"))
(load (concstring *home* "/quicklisp/setup.lisp"))
(set-module-dir (concstring *home* "/stumpwm-contrib"))
(setf *confdir* (concstring *home* "/.stumpwm.d/"))
(load (concstring *confdir* "/keybinds.lisp"))
(load (concstring *confdir* "/commands.lisp"))

(setf *default-group-name* "Emacs")

;; (load-module "ttf-fonts")
;; (ql:quickload "clx-truetype")
;; (defvar *fonts-cached* nil)
;; (unless *fonts-cached*
;;   (setf xft:*font-dirs* (list "/usr/share/fonts/truetype/"))
;; (setf xft::+font-cache-filename+ (format nil "/home/formbi/fonts/~a-font-cache.clx-truetype" (uiop:getenv "USER")))
;;   (xft:cache-fonts)
;;   (setf *fonts-cached* t))

;; (or
;;  (ignore-errors
;;    (progn
;;      (set-font (make-instance 'xft:font :family "DejaVu Sans Mono" :subfamily "Book" :size 10)) t))
;; (set-font "-*-*-r-*-20-*"))


(setf *mouse-focus-policy* :click)



(ql:quickload "str")
(ql:quickload "cl-ppcre")

;; (setf stumpwm:*screen-mode-line-format*
;;       (list
;;        "%g"
;;        "^>"
;;        		""))


;; (setf *mode-line-position* :top)
;; (setf *mode-line-timeout* 1)
;; (stumpwm:toggle-mode-line (stumpwm:current-screen)
;;                           (stumpwm:current-head))


(stumpwm:add-hook stumpwm:*destroy-window-hook*
                  #'(lambda (win) (stumpwm:repack-window-numbers)))

(load (concstring *confdir* "startup.lisp"))
(load (concstring *confdir* "multimedia-keys.lisp"))

(ql:quickload "zpng")
(load-module "alert-me")
(load-module "screenshot")
;; (ql:quickload "xml-emitter")
;; (ql:quickload "dbus")
;; (load-module "notify")
;; (notify:notify-server-toggle)


;; Local Variables:
;; eval: (stumpwm-mode)
;; eval: (paredit-mode)
;; End:
