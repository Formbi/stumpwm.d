(setf (getenv "GDK_CORE_DEVICE_EVENTS") "1")
(load (concstring *confdir* "/commands.lisp"))

;; (run-shell-command "setxkbmap -option 'ctrl:nocaps' -layout pl")
;; (run-shell-command (concstring *home* "/.fehbg"))
(run-shell-command (concstring *confdir* "/bash/startup-basic"))

(defcommand startup () () (run-shell-command (concstring *confdir* "/bash/startup")))


;; Local Variables:
;; eval: (stumpwm-mode)
;; eval: (paredit-mode)
;; End:
